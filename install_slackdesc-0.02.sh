#!/bin/sh
#
# Linux is a Registered Trademark of Linus Torvalds.
#
# Slackware is a registered trademark
# of Patrick Volkerding and Slackware Linux, Inc.
#
#==============================================================================
#
# Acknowledgments
#
# I've referred the "slack-desc" file for the post-installation message.
# Therefore, the license below does not apply to the message.
#
# Copyright (c) 2019 EarthSimilarityIndex, the 3rd Planet, Solar System
#
# Released under the MIT license
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

TIMESTAMP=$(date +%Y%m%d%H%M%S%N)

test -L / && exit 1
test -L /var && exit 1
test -L /var/log && exit 1
test -L /var/log/packages && exit 1
test -L /tmp && exit 1
test -L /tmp/build && exit 1
test -L /root && exit 1
test -L /root/Downloads && exit 1

if test ! "$USER" = root; then
  echo "Run this script as root, bye."
  exit 1
fi

test -d /root/Downloads || mkdir /root/Downloads
cd /root/Downloads/ || exit 1
rm "/root/Downloads/slackdesc-all-0.02-1spn" ||
rm -r "/root/Downloads/slackdesc-all-0.02-1spn"
rm "/root/Downloads/slackdesc-all-0.02-1spn.tgz" ||
rm -r "/root/Downloads/slackdesc-all-0.02-1spn.tgz"
wget https://sourceforge.net/projects/slack-desc/files/slackdesc-all-0.02-1spn.tgz \
	-O /root/Downloads/slackdesc-all-0.02-1spn.tgz || exit 1
mkdir /root/Downloads/slackdesc-all-0.02-1spn
tar -C /root/Downloads/slackdesc-all-0.02-1spn \
	-zxf /root/Downloads/slackdesc-all-0.02-1spn.tgz
cd /root/Downloads/slackdesc-all-0.02-1spn
mv /root/Downloads/slackdesc-all-0.02-1spn/install/slack-desc \
	/root/Downloads/slackdesc-all-0.02-1spn/install/slack-desc.orig
sed s/0.01/0.02/ /root/Downloads/slackdesc-all-0.02-1spn/install/slack-desc.orig \
	>>/root/Downloads/slackdesc-all-0.02-1spn/install/slack-desc
rm "/root/Downloads/slackdesc-all-0.02-1spn/install/slack-desc.orig"
mkdir -p /root/Downloads/slackdesc-all-0.02-1spn/usr/doc/slackdesc-0.02
wget https://sourceforge.net/projects/slack-desc/files/readme \
	-O /root/Downloads/slackdesc-all-0.02-1spn/usr/doc/slackdesc-0.02/README || exit 1
mv /root/Downloads/slackdesc-all-0.02-1spn/sbin \
	/root/Downloads/slackdesc-all-0.02-1spn/usr/
test -x /root/Downloads/slackdesc-all-0.02-1spn/usr/sbin/slackdesc ||
chmod +x /root/Downloads/slackdesc-all-0.02-1spn/usr/sbin/slackdesc
makepkg -l y -c y /root/Downloads/slackdesc-0.02-noarch-$TIMESTAMP.txz
upgradepkg /root/Downloads/slackdesc-0.02-noarch-$TIMESTAMP.txz ||
installpkg /root/Downloads/slackdesc-0.02-noarch-$TIMESTAMP.txz
cd || exit 1
cat <<EOS | less
slackdesc 0.02 slackware slack-desc generator tool

slackdesc is a tool that generates the slack-desc files for
Slackware linux packages. It supports two modes of operation:
the argument-based one and the interactive one.

http://slack-desc.sourceforge.net
EOS
exit 0
